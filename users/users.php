<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

use Phalcon\Http\Request,
    Phalcon\Http\Response;

function get_user($id_user = -1){
    $status_code = array(200, 'User Data');
    $response = new Response(); 
    if($id_user == null){
        $user = Users::find('role_id = 2 OR role_id = 3');
        $user = json_decode(json_encode($user));
        $data = array();
        foreach ($user as $u) {
            unset($u->password);
            $u->detail = UserDetail::findFirst("user_id = $u->id_user");
            $u->reference = References::find("employee_id = $u->id_user");
            $data[] = $u;
        }
        $content = json_encode($data);
    }else{
        $user = Users::findFirst($id_user);
        if($user == false){
            $content = json_encode(array(
                'status'   => 'ERROR',
                'messages' => "Usuario no encontrado"
            ));
        }else{
            $content = json_encode(array(
                'status'   => 'OK',
                'messages' => "Usuario encontrado",
                'user' =>   $user
            ));
        }
    }
    
    // Status Code
    $response->setStatusCode($status_code[0],$status_code[1]);
    // Mandamos el tipo de contenido al Header
    $response->setContentType('application/json');
    //Para acceder desde cualquier origen en el http request
    $response->setHeader('Access-Control-Allow-Origin', '*');
    // Asignamos los datos al contenido del header
    $response->setContent($content);
    // Regresamos la respuesta dada.
    return $response;
}


function add_user(){

    $request = new Request(); 
    // Get Data From Post
    $user_data = $request->getJsonRawBody(); // Convert Data To Object
    
    $nUser = new Users();
    $nUser->user = $user_data->user;
    $nUser->password = $user_data->password;
    $nUser->role_id = $user_data->role_id;
    $res = [];
    if($nUser->create() === false) {
        $res =  [
            'status'   => 'ERROR',
        ];
        $messages = $nUser->getMessages();
        $res['message'] = $messages[0]->getMessage();

        
    }else{
        $res =  [
            'status'   => 'OK',
            'messages' => "Usuario registrado con exito",
            'user' =>   $nUser
        ];
    }

    $status_code = array(200, 'User Created');
    $response = new Response(); 
    //Define the content 
    $content = json_encode($res);
    // Status Code
    $response->setStatusCode($status_code[0],$status_code[1]);
    // Mandamos el tipo de contenido al Header
    $response->setContentType('application/json');
    //Para acceder desde cualquier origen en el http request
    $response->setHeader('Access-Control-Allow-Origin', '*');
    // Asignamos los datos al contenido del header
    $response->setContent($content);
    // Regresamos la respuesta dada.
    return $response;

}
function update_user($id_user){
    if($id_user == null){
        exit(404);
    }
    $request = new Request(); // Get Data From Post
    $user_data =(array) $request->getJsonRawBody(); // Convert Data To 
    $user = Users::findFirst($id_user);
    $res = [];
    if($user->update($user_data) == false) {
        $res =  [
            'status'   => 'ERROR',
        ];
        $messages = $user->getMessages();
        $res['message'] = $messages[0]->getMessage();
        
    }else{
        $res =  [
            'status'   => 'OK',
            'messages' => "Usuario actualizado con exito",
            'user' =>   $user
        ];
    }
    $status_code = array(200, 'User Updated');
    $response = new Response(); 
    //Define the content 
    $content = json_encode($res);
    // Status Code
    $response->setStatusCode($status_code[0],$status_code[1]);
    // Mandamos el tipo de contenido al Header
    $response->setContentType('application/json');
    //Para acceder desde cualquier origen en el http request
    $response->setHeader('Access-Control-Allow-Origin', '*');
    // Asignamos los datos al contenido del header
    $response->setContent($content);
    // Regresamos la respuesta dada.
    return $response;

}

function delete_user($id_user){

    $status_code = array(200, 'User Deleted');
    $user = Users::findFirst($id_user);
    $res = array();
    if ($user !== false) {
        if ($user->delete() === false) {
            
            $res['status']='ERROR';
            $messages = $user->getMessages();
            $res['message'] = $messages[0]->getMessage();
            
        } else {

            $res['status'] = 'OK';
            $res['messages'] = "Usuario eliminado";
        }
    }else{
            $res['status'] = 'ERROR';
            $res['messages'] = "Usuario no encontrado";
    
    }
    $response = new Response(); 
    //Define the content 
    $content = json_encode($res);
    // Status Code
    $response->setStatusCode($status_code[0],$status_code[1]);
    // Mandamos el tipo de contenido al Header
    $response->setContentType('application/json');
    //Para acceder desde cualquier origen en el http request
    $response->setHeader('Access-Control-Allow-Origin', '*');
    // Asignamos los datos al contenido del header
    $response->setContent($content);
    // Regresamos la respuesta dada.
    return $response;

}



function login(){

    $request = new Request(); // Get Data From Post
    $user_data = $request->getJsonRawBody(); // Convert Data To Object
    $user = Users::findFirst("user = '$user_data->email'");
    $res = array();
    if($user == false){
        $res['status']   = 'ERROR';
        $res['messages'] = "No se encontro al usuario";
        $status_code = array(406, 'User not found');
    }else{
        if($user->comparePassword($user_data->password) == true){
            $res['status']   = 'OK';
            $res['messages'] = "Inicio de sesion exitoso";
            $user->password = null;
            $res['user'] =   $user;
            $res['user_detail'] = UserDetail::findFirst("user_id = '$user->id_user'");
            $status_code = array(200, 'Successful log in');
        }else{
            $res['status']   = 'ERROR';
            $res['messages'] = "Credenciales incorrectas";
            $status_code = array(406, 'Incorrect user / pasword');
        }
    }

    $response = new Response(); 
    //Define the content 
    $content = json_encode($res);
    // Status Code
    $response->setStatusCode($status_code[0],$status_code[1]);
    // Mandamos el tipo de contenido al Header
    $response->setContentType('application/json');
    //Para acceder desde cualquier origen en el http request
    $response->setHeader("Access-Control-Allow-Origin", '*');
        // Asignamos los datos al contenido del header
    $response->setContent($content);
    // Regresamos la respuesta dada.
    return $response;

}

function get_users_rol($rol){

    $users = Users::find("role_id = '$rol'");

    $users = json_decode(json_encode($users));
    $data = array();
    foreach ($users as $u) {
        unset($u->password);
        $u->detail = UserDetail::findFirst("user_id = $u->id_user");
        $u->reference = References::find("employee_id = $u->id_user");
        $data[] = $u;
    }
    $content = json_encode(array(
        'status'   => 'OK',
        'messages' => "Usuarios encontrados",
        'users' =>   $data
    ));
    $status_code = array(200, 'User Data');

    $response = new Response(); 
    // Status Code
    $response->setStatusCode($status_code[0],$status_code[1]);
    // Mandamos el tipo de contenido al Header
    $response->setContentType('application/json');
    //Para acceder desde cualquier origen en el http request
    $response->setHeader('Access-Control-Allow-Origin', '*');
    // Asignamos los datos al contenido del header
    $response->setContent($content);
    // Regresamos la respuesta dada.
    return $response;

}

function get_user_detail($id_user){
    $status_code = array(200, 'User Detail ');
    $response = new Response(); 
    
    $user = UserDetail::findFirst("user_id = '$id_user'");
    if($user == false){
        $content = json_encode(array(
            'status'   => 'ERROR',
            'messages' => "Usuario no encontrado"
        ));
    }else{
        $content = json_encode(array(
            'status'   => 'OK',
            'messages' => "Usuario encontrado",
            'detail' =>   $user
        ));
    }
    // Status Code
    $response->setStatusCode($status_code[0],$status_code[1]);
    // Mandamos el tipo de contenido al Header
    $response->setContentType('application/json');
    //Para acceder desde cualquier origen en el http request
    $response->setHeader('Access-Control-Allow-Origin', '*');
    // Asignamos los datos al contenido del header
    $response->setContent($content);
    // Regresamos la respuesta dada.
    return $response;
}
function add_user_detail($id_user){
    
    $request = new Request(); // Get Data From Post
    $user_data = (array) $request->getJsonRawBody(); // Convert Data To Object
    
    $user_detail = new UserDetail();
    $user_detail->user_id = $id_user;
    $res = [];
    if($user_detail->save($user_data) === false) {
        $res =  [
            'status'   => 'ERROR',
        ];
        $messages = $user_detail->getMessages();
        $res['message'] = $messages[0]->getMessage();        
    }else{
        $res =  [
            'status'   => 'OK',
            'messages' => "Detalle de usuario agregado con exito",
            'user_detail' =>   $user_detail
        ];
    }

    $status_code = array(200, 'User Detail Created');
    $response = new Response(); 
    //Define the content 
    $content = json_encode($res);
    // Status Code
    $response->setStatusCode($status_code[0],$status_code[1]);
    // Mandamos el tipo de contenido al Header
    $response->setContentType('application/json');
    //Para acceder desde cualquier origen en el http request
    $response->setHeader('Access-Control-Allow-Origin', '*');
    // Asignamos los datos al contenido del header
    $response->setContent($content);
    // Regresamos la respuesta dada.
    return $response;

}
function update_user_detail($id_user){
    if($id_user == null){
        exit(404);
    }
    $request = new Request(); // Get Data From Post
    $user_data =(array) $request->getJsonRawBody(); // Convert Data To 
    $user = UserDetail::findFirst("user_id = '$id_user'");
    $res = [];
    if($user->update($user_data) == false) {
        $res =  [
            'status'   => 'ERROR',
        ];
        $messages = $user->getMessages();
        $res['message'] = $messages[0]->getMessage();
        
    }else{
        $res =  [
            'status'   => 'OK',
            'messages' => "Detalles de usuario actualizados con exito",
            'user_detail' =>   $user
        ];
    }
    $status_code = array(200, 'User Detail Updated');
    $response = new Response(); 
    //Define the content 
    $content = json_encode($res);
    // Status Code
    $response->setStatusCode($status_code[0],$status_code[1]);
    // Mandamos el tipo de contenido al Header
    $response->setContentType('application/json');
    //Para acceder desde cualquier origen en el http request
    $response->setHeader('Access-Control-Allow-Origin', '*');
    // Asignamos los datos al contenido del header
    $response->setContent($content);
    // Regresamos la respuesta dada.
    return $response;
}
function delete_user_detail($id_user){
    $status_code = array(200, 'User Detail Deleted');
    $user = UserDetail::findFirst("user_id = '$id_user'");
    $res = array();
    if ($user !== false) {
        if ($user->delete() === false) {
            
            $res['status']='ERROR';
            $messages = $user->getMessages();
            $res['message'] = $messages[0]->getMessage();
            
        } else {

            $res['status'] = 'OK';
            $res['messages'] = "Detalle de usuario eliminado";
        }
    }else{
            $res['status'] = 'ERROR';
            $res['messages'] = "Usuario no encontrado";
    }
    $response = new Response(); 
    //Define the content 
    $content = json_encode($res);
    // Status Code
    $response->setStatusCode($status_code[0],$status_code[1]);
    // Mandamos el tipo de contenido al Header
    $response->setContentType('application/json');
    //Para acceder desde cualquier origen en el http request
    $response->setHeader('Access-Control-Allow-Origin', '*');
    // Asignamos los datos al contenido del header
    $response->setContent($content);
    // Regresamos la respuesta dada.
    return $response;
}

function register(){
    //company
    //insdustry
    $request = new Request(); // Get Data From Post
    $user_data = $request->getJsonRawBody(); // Convert Data To Object
    
    $nUser = new Users();
    $nUser->user = $user_data->user;
    $nUser->password = $user_data->password;
    $nUser->role_id = $user_data->role_id;
    $res = [];
    if($nUser->create() === false) {
        $res =  [
            'status'   => 'ERROR',
        ];
        $messages = $nUser->getMessages();
        $res['message'] = $messages[0]->getMessage();
    }else{

        $user_detail = new UserDetail();
        $user_detail->user_id = $nUser->id_user;
        $user_detail->first_name = $user_data->first_name;
        $user_detail->last_name = $user_data->last_name;

        if($nUser->role_id == 2){
            $user_detail->company_name = $user_data->company_name;
            $user_detail->industry = $user_data->industry;
        }else{
            $user_detail->work_type = $user_data->work_type;
        }
        
        
        if($user_detail->create() === false) {
            $res =  [
                'status'   => 'ERROR',
            ];
            $messages = $user_detail->getMessages();
            $res['message'] = $messages[0]->getMessage();        
        }else{
            $res =  [
                'status'   => 'OK',
                'messages' => "Usuario registrado con exito",
                'user' =>   $nUser,
                'user_detail' => $user_detail
            ];
        }
    }

    $status_code = array(200, 'Register');
    
    $response = new Response(); 
    //Define the content 
    $content = json_encode($res);
    // Status Code
    $response->setStatusCode($status_code[0],$status_code[1]);
    // Mandamos el tipo de contenido al Header
    $response->setContentType('application/json');
    //Para acceder desde cualquier origen en el http request
    $response->setHeader('Access-Control-Allow-Origin', '*');
    // Asignamos los datos al contenido del header
    $response->setContent($content);
    // Regresamos la respuesta dada.
    return $response;
}
?>