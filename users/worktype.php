<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

use Phalcon\Http\Request,
    Phalcon\Http\Response;

function get_user_worktype($id_user){
    $status_code = array(200, 'Work Type Data');
    $response = new Response(); 
    if($id_user == null){
        $content = json_encode(array(
            'status'   => 'ERROR',
            'messages' => 'Work Type not found'
        ));
    }else{
        $worktype = UserWorkType::find("employee_id = '$id_user'");
        $content = json_encode(array(
                'status'   => 'OK',
                'worktype' =>   $worktype
            ));
    }
    // Status Code
    $response->setStatusCode($status_code[0],$status_code[1]);
    // Mandamos el tipo de contenido al Header
    $response->setContentType('application/json');
    //Para acceder desde cualquier origen en el http request
    $response->setHeader('Access-Control-Allow-Origin', '*');
    // Asignamos los datos al contenido del header
    $response->setContent($content);
    // Regresamos la respuesta dada.
    return $response;
}

function add_user_worktype(){
    
    $request = new Request(); // Get Data From Post
    $worktype = (array) $request->getJsonRawBody(); // Convert Data To Object

    $nWorkType = new UserWorkType();
    $res = [];
    if($nWorkType->save($worktype) === false) {
        $res =  [
            'status'   => 'ERROR',
        ];
        $messages = $nWorkType->getMessages();
        $res['message'] = $messages[0]->getMessage();        
    }else{
        $res =  [
            'status'   => 'OK',
            'messages' => "Work Type Successfully Added ",
        ];
    }

    $status_code = array(200, 'Work Type Data');
    $response = new Response(); 
    //Define the content 
    $content = json_encode($res);
    // Status Code
    $response->setStatusCode($status_code[0],$status_code[1]);
    // Mandamos el tipo de contenido al Header
    $response->setContentType('application/json');
    //Para acceder desde cualquier origen en el http request
    $response->setHeader('Access-Control-Allow-Origin', '*');
    // Asignamos los datos al contenido del header
    $response->setContent($content);
    // Regresamos la respuesta dada.
    return $response;

}

function delete_user_worktype($id){
    $status_code = array(200, 'Work Type Delete');
    $worktype = UserWorkType::findFirst("id = '$id'");
    $res = array();
    if ($worktype !== false) {
        if ($worktype->delete() === false) {
            
            $res['status']='ERROR';
            $messages = $worktype->getMessages();
            $res['message'] = $messages[0]->getMessage();
            
        } else {
            $res['status'] = 'OK';
            $res['messages'] = "Work Type delete";
        }
    }else{
            $res['status'] = 'ERROR';
            $res['messages'] = "Work Type not found";
    }
    $response = new Response(); 
    //Define the content 
    $content = json_encode($res);
    // Status Code
    $response->setStatusCode($status_code[0],$status_code[1]);
    // Mandamos el tipo de contenido al Header
    $response->setContentType('application/json');
    //Para acceder desde cualquier origen en el http request
    $response->setHeader('Access-Control-Allow-Origin', '*');
    // Asignamos los datos al contenido del header
    $response->setContent($content);
    // Regresamos la respuesta dada.
    return $response;
}

?>