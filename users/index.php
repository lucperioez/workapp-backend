<?php

use Phalcon\Mvc\Micro;

include '../config.php';
include '../models/Users.php';
include 'users.php';
include 'employee_city.php';
include 'references.php';
include 'worktype.php';

$app = new Micro();


$app->before(function() use ($app) {
	$app->response->setHeader("Access-Control-Allow-Origin", '*')
		->setHeader("Access-Control-Allow-Methods", 'GET,PUT,POST,DELETE,OPTIONS')
		->setHeader("Access-Control-Allow-Headers", 'Origin, X-Requested-With, Content-Range, Content-Disposition, Content-Type');	
	$app->response->sendHeaders();
});
$app->options('/{catch:(.*)}', function() use ($app) { 
	$app->response->setStatusCode(200, "OK")->send();
});

$app->get('/{id_user}','get_user');
$app->post('/','add_user');
$app->put('/{id_user}','update_user');
$app->delete('/{id_user}','delete_user');
$app->get('/rol/{tipo}','get_users_rol');

$app->post('/login','login');
$app->post('/register','register');

$app->get('/detail/{id_user}','get_user_detail');
$app->post('/detail/{id_user}','add_user_detail');
$app->put('/detail/{id_user}','update_user_detail');
$app->delete('/detail/{id_user}','delete_user_detail');

$app->get('/employee/city/{id_employee}','get_employee_city');
$app->post('/employee/city/{id_employee}','add_employee_city');
$app->delete('/employee/city/{id_employee_city}','delete_employee_city');

$app->get('/reference/{id_user}','get_user_reference');
$app->post('/reference/{id_user}','add_user_reference');
$app->put('/reference/{id_reference}','update_user_reference');
$app->delete('/reference/{id_reference}','delete_user_reference');

$app->get('/worktype/{id_user}','get_user_worktype');
$app->post('/worktype','add_user_worktype');
$app->delete('/worktype/{id}','delete_user_worktype');


$app->notFound(function() use ($app){
	$app->response->setStatusCode(404, "Not Found");
	$app->response->sendHeaders();
	echo "That function is not here, call me, maybes!";

});

$app -> handle();