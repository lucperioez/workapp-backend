<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

use Phalcon\Http\Request,
    Phalcon\Http\Response;

function get_user_reference($id_user){
    $status_code = array(200, 'User Reference');
    $response = new Response(); 
    if($id_user == null){
        $content = json_encode(array(
            'status'   => 'ERROR',
            'messages' => 'User refernces not found'
        ));
    }else{
        $reference = References::find("employee_id = '$id_user'");
        $content = json_encode(array(
                'status'   => 'OK',
                'messages' => 'Employee cities',
                'refrences' =>   $reference
            ));
    }
    // Status Code
    $response->setStatusCode($status_code[0],$status_code[1]);
    // Mandamos el tipo de contenido al Header
    $response->setContentType('application/json');
    //Para acceder desde cualquier origen en el http request
    $response->setHeader('Access-Control-Allow-Origin', '*');
    // Asignamos los datos al contenido del header
    $response->setContent($content);
    // Regresamos la respuesta dada.
    return $response;
}

function add_user_reference($id_user){
    
    $request = new Request(); // Get Data From Post
    $reference = (array) $request->getJsonRawBody(); // Convert Data To Object

    $nReferences = new References();
    $nReferences->employee_id = $id_user;
    $res = [];
    if($nReferences->save($reference) === false) {
        $res =  [
            'status'   => 'ERROR',
        ];
        $messages = $nReferences->getMessages();
        $res['message'] = $messages[0]->getMessage();        
    }else{
        $res =  [
            'status'   => 'OK',
            'messages' => "Reference Successfully Added ",
        ];
    }

    $status_code = array(200, 'References');
    $response = new Response(); 
    //Define the content 
    $content = json_encode($res);
    // Status Code
    $response->setStatusCode($status_code[0],$status_code[1]);
    // Mandamos el tipo de contenido al Header
    $response->setContentType('application/json');
    //Para acceder desde cualquier origen en el http request
    $response->setHeader('Access-Control-Allow-Origin', '*');
    // Asignamos los datos al contenido del header
    $response->setContent($content);
    // Regresamos la respuesta dada.
    return $response;

}

function update_user_reference($id_reference){
    if($id_reference == null){
        exit(404);
    }
    $request = new Request(); // Get Data From Post
    $reference_data = (array) $request->getJsonRawBody(); // Convert Data To 
    $reference = References::findFirst("id_reference = '$id_reference'");
    $res = [];
    if($reference->update($reference_data) == false) {
        $res =  [
            'status'   => 'ERROR',
        ];
        $messages = $reference->getMessages();
        $res['message'] = $messages[0]->getMessage();
        
    }else{
        $res =  [
            'status'   => 'OK',
            'messages' => "Reference Successfully Update",
            'reference' =>   $reference
        ];
    }
    $status_code = array(200, 'Reference Updated');
    $response = new Response(); 
    //Define the content 
    $content = json_encode($res);
    // Status Code
    $response->setStatusCode($status_code[0],$status_code[1]);
    // Mandamos el tipo de contenido al Header
    $response->setContentType('application/json');
    //Para acceder desde cualquier origen en el http request
    $response->setHeader('Access-Control-Allow-Origin', '*');
    // Asignamos los datos al contenido del header
    $response->setContent($content);
    // Regresamos la respuesta dada.
    return $response;
}


function delete_user_reference($id_reference){
    $status_code = array(200, 'Reference Delete');
    $reference = References::findFirst("id_reference = '$id_reference'");
    $res = array();
    if ($reference !== false) {
        if ($reference->delete() === false) {
            
            $res['status']='ERROR';
            $messages = $reference->getMessages();
            $res['message'] = $messages[0]->getMessage();
            
        } else {
            $res['status'] = 'OK';
            $res['messages'] = "Reference delete";
        }
    }else{
            $res['status'] = 'ERROR';
            $res['messages'] = "Reference not found";
    }
    $response = new Response(); 
    //Define the content 
    $content = json_encode($res);
    // Status Code
    $response->setStatusCode($status_code[0],$status_code[1]);
    // Mandamos el tipo de contenido al Header
    $response->setContentType('application/json');
    //Para acceder desde cualquier origen en el http request
    $response->setHeader('Access-Control-Allow-Origin', '*');
    // Asignamos los datos al contenido del header
    $response->setContent($content);
    // Regresamos la respuesta dada.
    return $response;
}

?>