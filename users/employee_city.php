<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

use Phalcon\Http\Request,
    Phalcon\Http\Response;

function get_employee_city($id_employee){
    $status_code = array(200, 'Employee City');
    $response = new Response(); 
    if($id_employee == null){
        //$employee_city = EmployeeCities::find();
        //$content = json_encode($employee_city);
        $content = json_encode(array(
            'status'   => 'ERROR',
            'messages' => 'Employee cities not found'
        ));
    }else{
        $employee_city = EmployeeCities::find("employee_id = '$id_employee'");
        $content = json_encode(array(
                'status'   => 'OK',
                'messages' => 'Employee cities',
                'employee_city' =>   $employee_city
            ));
    }
    // Status Code
    $response->setStatusCode($status_code[0],$status_code[1]);
    // Mandamos el tipo de contenido al Header
    $response->setContentType('application/json');
    //Para acceder desde cualquier origen en el http request
    $response->setHeader('Access-Control-Allow-Origin', '*');
    // Asignamos los datos al contenido del header
    $response->setContent($content);
    // Regresamos la respuesta dada.
    return $response;
}

function add_employee_city($id_employee){
    
    $request = new Request(); // Get Data From Post
    $city = (array) $request->getJsonRawBody(); // Convert Data To Object
    
    $employee_city = new EmployeeCities();
    $employee_city->employee_id = $id_employee;
    $res = [];
    if($employee_city->save($city) === false) {
        $res =  [
            'status'   => 'ERROR',
        ];
        $messages = $employee_city->getMessages();
        $res['message'] = $messages[0]->getMessage();        
    }else{
        $res =  [
            'status'   => 'OK',
            'messages' => "City Successfully Added ",
        ];
    }

    $status_code = array(200, 'Employee City');
    $response = new Response(); 
    //Define the content 
    $content = json_encode($res);
    // Status Code
    $response->setStatusCode($status_code[0],$status_code[1]);
    // Mandamos el tipo de contenido al Header
    $response->setContentType('application/json');
    //Para acceder desde cualquier origen en el http request
    $response->setHeader('Access-Control-Allow-Origin', '*');
    // Asignamos los datos al contenido del header
    $response->setContent($content);
    // Regresamos la respuesta dada.
    return $response;

}

function delete_employee_city($id_employee_city){
    $status_code = array(200, 'Employee City');
    $employee_city = EmployeeCities::findFirst("id_employee_city = '$id_employee_city'");
    $res = array();
    if ($employee_city !== false) {
        if ($employee_city->delete() === false) {
            
            $res['status']='ERROR';
            $messages = $employee_city->getMessages();
            $res['message'] = $messages[0]->getMessage();
            
        } else {
            $res['status'] = 'OK';
            $res['messages'] = "Employe city delete";
        }
    }else{
            $res['status'] = 'ERROR';
            $res['messages'] = "Employee city not found";
    }
    $response = new Response(); 
    //Define the content 
    $content = json_encode($res);
    // Status Code
    $response->setStatusCode($status_code[0],$status_code[1]);
    // Mandamos el tipo de contenido al Header
    $response->setContentType('application/json');
    //Para acceder desde cualquier origen en el http request
    $response->setHeader('Access-Control-Allow-Origin', '*');
    // Asignamos los datos al contenido del header
    $response->setContent($content);
    // Regresamos la respuesta dada.
    return $response;
}

?>