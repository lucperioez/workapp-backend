<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

include '../models/Adwords.php';

use Phalcon\Http\Request,
    Phalcon\Http\Response;

function get_adwords(){
    $status_code = array(200, 'Adwords Data');
    $response = new Response(); 
    $adwords = Adwords::find();
    $content = json_encode($adwords); 
    // Status Code
    $response->setStatusCode($status_code[0],$status_code[1]);
    // Mandamos el tipo de contenido al Header
    $response->setContentType('application/json');
    //Para acceder desde cualquier origen en el http request
    $response->setHeader('Access-Control-Allow-Origin', '*');
    // Asignamos los datos al contenido del header
    $response->setContent($content);
    // Regresamos la respuesta dada.
    return $response;
}


function add_adwords(){
    $request = new Request(); // Get Data From Post
    $adword_data = (array) $request->getJsonRawBody(); // Convert Data To Object
    $formato = '';
    if (strpos($adword_data['imagen'], 'data:image/png;base64') !== false) {
        $img = str_replace('data:image/png;base64,', '', $adword_data['imagen']);
        $formato = '.png';
    }
    if (strpos($adword_data['imagen'], 'data:image/jpeg;base64') !== false) {
        $img = str_replace('data:image/jpeg;base64,', '', $adword_data['imagen']);
        $formato = '.jpeg';
    }
    $img = str_replace(' ', '+', $img);
    $data = base64_decode($img);
    $name = uniqid() . $formato;
    $file = '../images-adwords/' . $name;
    $nAdword = new Adwords();
    $nAdword->url = "/images-adwords/".$name;
    $nAdword->action = $adword_data['url'];
    $res = [];
    if($nAdword->save() === false) {
        $res =  [
            'status'   => 'ERROR',
        ];
        $messages = $nAdword->getMessages();
        $res['message'] = $messages[0]->getMessage();
    }else{    
        $success = file_put_contents($file, $data);
        $res =  [
            'status'   => 'OK',
            'messages' => "Adword added",
            'adword' =>   $nAdword
        ];
    }

    $status_code = array(200, 'Adword Created');
    $response = new Response(); 
    //Define the content 
    $content = json_encode($res);
    // Status Code
    $response->setStatusCode($status_code[0],$status_code[1]);
    // Mandamos el tipo de contenido al Header
    $response->setContentType('application/json');
    //Para acceder desde cualquier origen en el http request
    $response->setHeader('Access-Control-Allow-Origin', '*');
    // Asignamos los datos al contenido del header
    $response->setContent($content);
    // Regresamos la respuesta dada.
    return $response;

}

function delete_adword($id_adword){

    $status_code = array(200, 'Adword Deleted');
    $adword = Adwords::findFirst($id_adword);
    $res = array();
    if ($adword !== false) {
        unlink("../".$adword->url);
        if ($adword->delete() === false) {
            $res['status']='ERROR';
            $messages = $adword->getMessages();
            $res['message'] = $messages[0]->getMessage();
        } else {
            $res['status'] = 'OK';
            $res['messages'] = "Adword Deleted";
        }
    }else{
            $res['status'] = 'ERROR';
            $res['messages'] = "Adword not found";
    
    }
    $response = new Response(); 
    //Define the content 
    $content = json_encode($res);
    // Status Code
    $response->setStatusCode($status_code[0],$status_code[1]);
    // Mandamos el tipo de contenido al Header
    $response->setContentType('application/json');
    //Para acceder desde cualquier origen en el http request
    $response->setHeader('Access-Control-Allow-Origin', '*');
    // Asignamos los datos al contenido del header
    $response->setContent($content);
    // Regresamos la respuesta dada.
    return $response;

}

?>