<?php
use Phalcon\Db\Adapter\Pdo\Mysql as DbAdapter;
use Phalcon\Di\FactoryDefault;



// Crear un DI
$di = new FactoryDefault();
// Configurar servicio de base de datos
$di->set(
    'db',
    function () {
        return new DbAdapter(
            [
                'host'     => '138.68.56.247',
                'username' => 'root',
                'password' => 'WorkApp***m',
                'dbname'   => 'workapp',
            ]
            /*[
                'host'     => 'localhost',
                'username' => 'root',
                'password' => '',
                'dbname'   => 'workapp',
            ]*/
        );
    }
);


?>
