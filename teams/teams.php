<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

include '../models/Teams.php';
include '../models/Users.php';
include '../models/OfferTeam.php';

use Phalcon\Http\Request,
    Phalcon\Http\Response;

function get_team($id_team = -1){
    $status_code = array(200, 'Team Data');
    $response = new Response(); 
    if($id_team == null){
        $content = json_encode(array(
            'status'   => 'ERROR',
            'messages' => "Team not found"
        ));
    }else{
        $team = Team::find("id_team = $id_team");
        if($team == false){
            $content = json_encode(array(
                'status'   => 'ERROR',
                'messages' => "Team not found"
            ));
        }else{
            $team = json_decode(json_encode($team));
            foreach ($team as $t) {
                $t->employes = UserDetail::findFirst("user_id = $u->employee_id");
                $data[] = $t;
            }
            $content = json_encode(array(
                'status'   => 'OK',
                'messages' => "Team found",
                'team' =>   $data
            ));
        }
    }
    
    // Status Code
    $response->setStatusCode($status_code[0],$status_code[1]);
    // Mandamos el tipo de contenido al Header
    $response->setContentType('application/json');
    //Para acceder desde cualquier origen en el http request
    $response->setHeader('Access-Control-Allow-Origin', '*');
    // Asignamos los datos al contenido del header
    $response->setContent($content);
    // Regresamos la respuesta dada.
    return $response;
}

function add_member(){
    
    $request = new Request(); // Get Data From Post
    $team_data = (array) $request->getJsonRawBody(); // Convert Data To Object
    //buscar que no exista el empleados
    $employe = $team_data['employee_id'];
    $builder = $team_data['builder_id'];
    $emp = Team::findFirst(["employee_id = $employe", "builder_id = $builder"]);
    if($emp == false){
        $nTeam = new Team();
        $res = [];
        if($nTeam->save($team_data) === false) {
            $res =  [
                'status'   => 'ERROR',
            ];
            $messages = $nTeam->getMessages();
            $res['message'] = $messages[0]->getMessage();
        }else{
            $res =  [
                'status'   => 'OK',
                'messages' => "Team added",
                'team' =>   $nTeam
            ];
        }
        $status_code = array(200, 'Member Added');
    }else{ 
        $res =  [
            'status'   => 'ERROR',
            'messages' => "The member is already in the team",
        ];
        $status_code = array(200, 'Member error');
    }
    $response = new Response(); 
    //Define the content 
    $content = json_encode($res);
    // Status Code
    $response->setStatusCode($status_code[0],$status_code[1]);
    // Mandamos el tipo de contenido al Header
    $response->setContentType('application/json');
    //Para acceder desde cualquier origen en el http request
    $response->setHeader('Access-Control-Allow-Origin', '*');
    // Asignamos los datos al contenido del header
    $response->setContent($content);
    // Regresamos la respuesta dada.
    return $response;

}

function delete_member($id_team){

    $status_code = array(200, 'Member Deleted');
    $team = Team::findFirst($id_team);
    $res = array();
    if ($team !== false) {
        if ($team->delete() === false) {
            
            $res['status']='ERROR';
            $messages = $team->getMessages();
            $res['message'] = $messages[0]->getMessage();
            
        } else {

            $res['status'] = 'OK';
            $res['messages'] = "Team Deleted";
        }
    }else{
            $res['status'] = 'ERROR';
            $res['messages'] = "Team not found";
    
    }
    $response = new Response(); 
    //Define the content 
    $content = json_encode($res);
    // Status Code
    $response->setStatusCode($status_code[0],$status_code[1]);
    // Mandamos el tipo de contenido al Header
    $response->setContentType('application/json');
    //Para acceder desde cualquier origen en el http request
    $response->setHeader('Access-Control-Allow-Origin', '*');
    // Asignamos los datos al contenido del header
    $response->setContent($content);
    // Regresamos la respuesta dada.
    return $response;

}

function get_team_members($id_builder){
    $status_code = array(200, 'Team Data');
    $response = new Response(); 
    if($id_builder == null){
        $content = json_encode(array(
            'status'   => 'ERROR',
            'messages' => "Team not found"
        ));
    }else{
        $team = Team::find("builder_id = $id_builder");
        if($team == false){
            $content = json_encode(array(
                'status'   => 'ERROR',
                'messages' => "Team not found"
            ));
        }else{
            $data = [];
            $team = json_decode(json_encode($team));
            foreach ($team as $t) {
                $t->employe = UserDetail::findFirst("user_id = $t->employee_id");
                $t->employe->checked = false;
                $data[] = $t;
            }
            $content = json_encode(array(
                'status'   => 'OK',
                'messages' => "Team found",
                'team' =>   $data
            ));
        }
    }
    
    // Status Code
    $response->setStatusCode($status_code[0],$status_code[1]);
    // Mandamos el tipo de contenido al Header
    $response->setContentType('application/json');
    //Para acceder desde cualquier origen en el http request
    $response->setHeader('Access-Control-Allow-Origin', '*');
    // Asignamos los datos al contenido del header
    $response->setContent($content);
    // Regresamos la respuesta dada.
    return $response;
}

?>