<?php

use Phalcon\Mvc\Micro;

include '../config.php';
include 'offerteam.php';

$app = new Micro();

$app->before(function() use ($app) {
	$app->response->setHeader("Access-Control-Allow-Origin", '*')
		->setHeader("Access-Control-Allow-Methods", 'GET,PUT,POST,DELETE,OPTIONS')
		->setHeader("Access-Control-Allow-Headers", 'Origin, X-Requested-With, Content-Range, Content-Disposition, Content-Type');	
	$app->response->sendHeaders();

});
$app->options('/{catch:(.*)}', function() use ($app) { 
	$app->response->setStatusCode(200, "OK")->send();
});

$app->post('/','add_offer_team2');
$app->delete('/{id}','delete_offer_team');
$app->get('/builder/{id_builder}','get_by_builder');
$app->get('/employe/{id_employe}','get_by_employe');
//$app->post('/many','add_offer_team2');

$app->notFound(function() use ($app){
	$app->response->setStatusCode(404, "Not Found");
	$app->response->sendHeaders();
	echo "That function is not here, call me, maybes!";

});

$app -> handle();