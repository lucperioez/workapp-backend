<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

include '../models/Teams.php';
include '../models/Users.php';
include '../models/Offers.php';
include '../models/OfferTeam.php';


use Phalcon\Http\Request,
    Phalcon\Http\Response;
use Phalcon\Db\Column;
function add_offer_team(){
    
    $request = new Request(); // Get Data From Post
    $data = (array) $request->getJsonRawBody(); // Convert Data To Object
    $nOfferTeam = new OfferTeam();
    $res = [];
    if($nOfferTeam->save($data) === false) {
        $res =  [
            'status'   => 'ERROR',
        ];
        $messages = $nOfferTeam->getMessages();
        $res['message'] = $messages[0]->getMessage();
    }else{
        $res =  [
            'status'   => 'OK',
            'messages' => "Offer added to team",
            'data' =>   $nOfferTeam
        ];
    }
    $status_code = array(200, 'Offer Added Team');
    $response = new Response(); 
    //Define the content 
    $content = json_encode($res);
    // Status Code
    $response->setStatusCode($status_code[0],$status_code[1]);
    // Mandamos el tipo de contenido al Header
    $response->setContentType('application/json');
    //Para acceder desde cualquier origen en el http request
    $response->setHeader('Access-Control-Allow-Origin', '*');
    // Asignamos los datos al contenido del header
    $response->setContent($content);
    // Regresamos la respuesta dada.
    return $response;

}

function delete_offer_team($id){

    $status_code = array(200, 'Offer Team Deleted');
    $offer_team = OfferTeam::findFirst($id);
    $res = array();
    if ($offer_team !== false) {
        if ($offer_team->delete() === false) {
            
            $res['status']='ERROR';
            $messages = $offer_team->getMessages();
            $res['message'] = $messages[0]->getMessage();
            
        } else {

            $res['status'] = 'OK';
            $res['messages'] = "Offer Team Deleted";
        }
    }else{
            $res['status'] = 'ERROR';
            $res['messages'] = "Offer Team not found";
    
    }
    $response = new Response(); 
    //Define the content 
    $content = json_encode($res);
    // Status Code
    $response->setStatusCode($status_code[0],$status_code[1]);
    // Mandamos el tipo de contenido al Header
    $response->setContentType('application/json');
    //Para acceder desde cualquier origen en el http request
    $response->setHeader('Access-Control-Allow-Origin', '*');
    // Asignamos los datos al contenido del header
    $response->setContent($content);
    // Regresamos la respuesta dada.
    return $response;

}

function get_by_builder($id_builder){
    $status_code = array(200, 'Offer Team Data');
    $response = new Response(); 
    if($id_builder == null){
        $content = json_encode(array(
            'status'   => 'ERROR',
            'messages' => "Offer Team not found"
        ));
    }else{
        $offer_team = OfferTeam::find([
            [
                'conditions' => "builder_id = $id_builder",
                'order'      => 'date DESC'
            ]
        ]);
        if($offer_team == false){
            $content = json_encode(array(
                'status'   => 'ERROR',
                'messages' => "Offer Team not found"
            ));
        }else{
            $data = [];
            $offer_team = json_decode(json_encode($offer_team));
            foreach ($offer_team as $o) {
                $o->offer = Offers::findFirst("id_offer_offers = $t->offer_id");
                $data[] = $o;
            }
            $content = json_encode(array(
                'status'   => 'OK',
                'messages' => "Offer Team found",
                'team' =>   $data
            ));
        }
    }
    
    // Status Code
    $response->setStatusCode($status_code[0],$status_code[1]);
    // Mandamos el tipo de contenido al Header
    $response->setContentType('application/json');
    //Para acceder desde cualquier origen en el http request
    $response->setHeader('Access-Control-Allow-Origin', '*');
    // Asignamos los datos al contenido del header
    $response->setContent($content);
    // Regresamos la respuesta dada.
    return $response;
}

function get_by_employe($id_employe){
    $status_code = array(200, 'Offer Team Data');
    $response = new Response(); 
    if($id_employe == null){
        $content = json_encode(array(
            'status'   => 'ERROR',
            'messages' => "Offer Team not found"
        ));
    }else{

        $teams = Team::find([
                'column'     => 'id_team',
                'conditions' => "employee_id = $id_employe"
            ]);
        $teams = json_decode(json_encode($teams));
        $offer_team = OfferTeam::find([
            [
                'conditions' => "team_id IN (:teams:)",
                'bind' => array('teams' =>$teams),
                'order'      => 'date DESC'
            ]
        ]);
        if($offer_team == false){
            $content = json_encode(array(
                'status'   => 'ERROR',
                'messages' => "Offer Team not found"
            ));
        }else{
            $data = [];
            $offer_team = json_decode(json_encode($offer_team));
            foreach ($offer_team as $o) {
                $o->offer = Offers::findFirst("id_offer = $o->offer_id");
                $data[] = $o;
            }
            $content = json_encode(array(
                'status'   => 'OK',
                'messages' => "Offer Team found",
                'team' =>   $data
            ));
        }
    }
    
    // Status Code
    $response->setStatusCode($status_code[0],$status_code[1]);
    // Mandamos el tipo de contenido al Header
    $response->setContentType('application/json');
    //Para acceder desde cualquier origen en el http request
    $response->setHeader('Access-Control-Allow-Origin', '*');
    // Asignamos los datos al contenido del header
    $response->setContent($content);
    // Regresamos la respuesta dada.
    return $response;
}

function add_offer_team2(){
    
    $request = new Request(); // Get Data From Post
    $data = $request->getJsonRawBody(); // Convert Data To Object
    foreach ($data->offers_teams as $offerteam) {
        $nOfferTeam = new OfferTeam();
        $nOfferTeam->builder_id = $offerteam->builder_id;
        $nOfferTeam->team_id = $offerteam->team_id;
        $nOfferTeam->offer_id = $offerteam->offer_id;
        $nOfferTeam->save();
    }

    $res =  [
        'status'   => 'OK',
        'messages' => "Offer added to teams" 
    ];
    $status_code = array(200, 'Offer Added Team');
    $response = new Response(); 
    //Define the content 
    $content = json_encode($res);
    // Status Code
    $response->setStatusCode($status_code[0],$status_code[1]);
    // Mandamos el tipo de contenido al Header
    $response->setContentType('application/json');
    //Para acceder desde cualquier origen en el http request
    $response->setHeader('Access-Control-Allow-Origin', '*');
    // Asignamos los datos al contenido del header
    $response->setContent($content);
    // Regresamos la respuesta dada.
    return $response;

}
?>