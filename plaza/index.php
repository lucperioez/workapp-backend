<?php

use Phalcon\Mvc\Micro;

include '../config.php';
include 'plaza.php';


$app = new Micro();

$app->before(function() use ($app) {
	$app->response->setHeader("Access-Control-Allow-Origin", '*')
		->setHeader("Access-Control-Allow-Methods", 'GET,PUT,POST,DELETE,OPTIONS')
		->setHeader("Access-Control-Allow-Headers", 'Origin, X-Requested-With, Content-Range, Content-Disposition, Content-Type');	
	$app->response->sendHeaders();

});
$app->options('/{catch:(.*)}', function() use ($app) { 
	$app->response->setStatusCode(200, "OK")->send();
});

$app->get('/{id_plaza}','get_plaza');
$app->post('/','add_plaza');
$app->put('/{id_plaza}','update_plaza');
$app->delete('/{id_plazaa}','delete_plaza');

$app->get('/offer/{id_offer}','get_plaza_by_offer');


$app->notFound(function() use ($app){
	$app->response->setStatusCode(404, "Not Found");
	$app->response->sendHeaders();
	echo "That function is not here, call me, maybes!";

});

$app -> handle();