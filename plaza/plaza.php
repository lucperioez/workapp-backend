<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

include '../models/Plaza.php';

use Phalcon\Http\Request,
    Phalcon\Http\Response;

function get_plaza($id_plaza = -1){
    $status_code = array(200, 'Plaza Data');
    $response = new Response(); 
    if($id_plaza == null){
        $plazas = Plaza::find();
        $content = json_encode($plazas);
    }else{
        $plaza = Plaza::findFirst($id_plaza);
        if($plaza == false){
            $content = json_encode(array(
                'status'   => 'ERROR',
                'messages' => "Plaza not found"
            ));
        }else{
            $content = json_encode(array(
                'status'   => 'OK',
                'messages' => "Plaza found",
                'plaza' =>   $plaza
            ));
        }
    }
    
    // Status Code
    $response->setStatusCode($status_code[0],$status_code[1]);
    // Mandamos el tipo de contenido al Header
    $response->setContentType('application/json');
    //Para acceder desde cualquier origen en el http request
    $response->setHeader('Access-Control-Allow-Origin', '*');
    // Asignamos los datos al contenido del header
    $response->setContent($content);
    // Regresamos la respuesta dada.
    return $response;
}


function add_plaza(){
    
    $request = new Request(); // Get Data From Post
    $plaza_data = (array) $request->getJsonRawBody(); // Convert Data To Object
    
    $nPlaza = new Plaza();
    $res = [];
    if($nPlaza->save($plaza_data) === false) {
        $res =  [
            'status'   => 'ERROR',
        ];
        $messages = $nPlaza->getMessages();
        $res['message'] = $messages[0]->getMessage();
    }else{
        $res =  [
            'status'   => 'OK',
            'messages' => "Plaza added",
            'plaza' =>   $nPlaza
        ];
    }

    $status_code = array(200, 'Plaza Created');
    $response = new Response(); 
    //Define the content 
    $content = json_encode($res);
    // Status Code
    $response->setStatusCode($status_code[0],$status_code[1]);
    // Mandamos el tipo de contenido al Header
    $response->setContentType('application/json');
    //Para acceder desde cualquier origen en el http request
    $response->setHeader('Access-Control-Allow-Origin', '*');
    // Asignamos los datos al contenido del header
    $response->setContent($content);
    // Regresamos la respuesta dada.
    return $response;

}
function update_offer($id_plaza){
    if($id_plaza == null){
        exit(404);
    }
    $request = new Request(); // Get Data From Post
    $plaza_data = (array) $request->getJsonRawBody(); // Convert Data To 
    $plaza = Plaza::findFirst($id_plaza);
    $res = [];
    if($plaza->update($plaza_data) == false) {
        $res =  [
            'status'   => 'ERROR',
        ];
        $messages = $plaza->getMessages();
        $res['message'] = $messages[0]->getMessage();
        
    }else{
        $res =  [
            'status'   => 'OK',
            'messages' => "Plaza Updated",
            'plaza' =>   $plaza
        ];
    }
    $status_code = array(200, 'Plaza Updated');
    $response = new Response(); 
    //Define the content 
    $content = json_encode($res);
    // Status Code
    $response->setStatusCode($status_code[0],$status_code[1]);
    // Mandamos el tipo de contenido al Header
    $response->setContentType('application/json');
    //Para acceder desde cualquier origen en el http request
    $response->setHeader('Access-Control-Allow-Origin', '*');
    // Asignamos los datos al contenido del header
    $response->setContent($content);
    // Regresamos la respuesta dada.
    return $response;
}

function delete_plaza($id_plaza){

    $status_code = array(200, 'Plaza Deleted');
    $plaza = Offers::findFirst($id_plaza);
    $res = array();
    if ($plaza !== false) {
        if ($plaza->delete() === false) {
            
            $res['status']='ERROR';
            $messages = $plaza->getMessages();
            $res['message'] = $messages[0]->getMessage();
            
        } else {

            $res['status'] = 'OK';
            $res['messages'] = "Plaza Deleted";
        }
    }else{
            $res['status'] = 'ERROR';
            $res['messages'] = "Plaza not found";
    
    }
    $response = new Response(); 
    //Define the content 
    $content = json_encode($res);
    // Status Code
    $response->setStatusCode($status_code[0],$status_code[1]);
    // Mandamos el tipo de contenido al Header
    $response->setContentType('application/json');
    //Para acceder desde cualquier origen en el http request
    $response->setHeader('Access-Control-Allow-Origin', '*');
    // Asignamos los datos al contenido del header
    $response->setContent($content);
    // Regresamos la respuesta dada.
    return $response;

}
function get_plaza_by_offer($id_offer = -1){
    
    $status_code = array(200, 'Plaza Data');
    $response = new Response(); 
    if($id_offer == null){
        $content = json_encode(array(
            'status'   => 'ERROR',
            'messages' => "Plaza not found"
        ));
    }else{
        $plazas = Plaza::find("offer_id = '$id_offer'");
        $content = json_encode($plazas);
    }
    
    // Status Code
    $response->setStatusCode($status_code[0],$status_code[1]);
    // Mandamos el tipo de contenido al Header
    $response->setContentType('application/json');
    //Para acceder desde cualquier origen en el http request
    $response->setHeader('Access-Control-Allow-Origin', '*');
    // Asignamos los datos al contenido del header
    $response->setContent($content);
    // Regresamos la respuesta dada.
    return $response;
}
?>