<?php
    use Phalcon\Mvc\Model;
    use Phalcon\Mvc\Model\Message;
    use Phalcon\Validation;
    use Phalcon\Validation\Validator\Uniqueness;
    use Phalcon\Security;


    class Province extends Model {

        public $id_province;
        public $name;
        public $status;

        public function initialize(){
            $this->setSource('province');
            $this->useDynamicUpdate(true);
            $this->keepSnapshots(true);
            $this->status = 1;
        } 
    }
    
    class City extends Model{
        public $id_city;
        public $name;
        public $status;
        public $province_id;

        public function initialize(){
            $this->setSource('city');
            $this->useDynamicUpdate(true);
            $this->keepSnapshots(true);
            $this->belongsTo(
                'province_id',
                'Province',
                'id_province'
            );
            $this->status = 1;
        }
    }

?>