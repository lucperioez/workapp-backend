<?php
    use Phalcon\Mvc\Model;
    use Phalcon\Mvc\Model\Message;
    use Phalcon\Validation;
    use Phalcon\Validation\Validator\Uniqueness;
    use Phalcon\Security;

    class OfferTeam extends Model {

        public $id;
        public $builder_id;
        public $team_id;
        public $offer_id;
        public $date;

        public function initialize(){
            $this->setSource('offer_team');
            $this->useDynamicUpdate(true);
            $this->keepSnapshots(true);
            $this->date = date('Y-m-d h:i:s');
            $this->belongsTo(
                'builder_id',
                'Users',
                'id_users'
            );
            $this->belongsTo(
                'team_id',
                'Team',
                'id_team'
            );
            $this->belongsTo(
                'offer_id',
                'Offers',
                'id_offer_offers'
            );
        } 
        public function beforeSave(){
            $this->date = date('Y-m-d h:i:s');
        }
    }



?>