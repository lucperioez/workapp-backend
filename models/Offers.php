<?php
    use Phalcon\Mvc\Model;
    use Phalcon\Mvc\Model\Message;
    use Phalcon\Validation;
    use Phalcon\Validation\Validator\Uniqueness;
    use Phalcon\Security;

    class OfferChat extends Model{
        public $id_offer_chat;
        public $message;
        public $created_date;
        public $offer_id;

        public function initialize(){
            $this->setSource('offer_chat');
            $this->useDynamicUpdate(true);
            $this->keepSnapshots(true);
            $this->created_date = date('Y-m-d h:i:s');
        }
    }

    class Offers extends Model {

        const PUBLISH_ALL = 1;
        const PUBLISH_TEAM = 2;
        const PUBLISH_NONE = 0;

        const NOT_COMPLETED = 0;
        const COMPLETED = 1;

        const ACTIVE = 1;
        const INACTIVE = 0;

        public $id_offer;
        public $title;
        public $address;
        public $state;
        public $city;
        public $hour_price;
        public $start_date;
        public $end_date;
        public $start_hour;
        public $end_hour;
        public $description;
        public $builder_id;
        public $created_date;
        public $status;
        public $published;
        public $completed;

        public function initialize(){
            $this->setSource('offers');
            $this->useDynamicUpdate(true);
            $this->keepSnapshots(true);
            $this->belongsTo(
                'builder_id',
                'Users',
                'id_users'
            );
            $this->status = 1;
            $this->completed = 0;
        } 
         
        public function beforeSave(){
            $this->created_date = date('Y-m-d h:i:s');
        }
    }

    class PostulatedOffers extends Model {
        
        const ACEPTED = 1;
        const NOT_ACEPTED = 0;

        public $id;
        public $offer_id;
        public $employe_id;
        public $builder_id;
        public $status;
        public $created_date;

        public function initialize(){
            $this->setSource('postulated_offers');
            $this->useDynamicUpdate(true);
            $this->keepSnapshots(true);
            $this->belongsTo(
                'offer_id',
                'Offers',
                'id_offer'
            );
            $this->belongsTo(
                'employe_id',
                'Users',
                'id_users'
            );
            $this->belongsTo(
                'builder_id',
                'Users',
                'id_users'
            );
            $this->status = 0;
            $this->created_date = date('Y-m-d h:i:s');
        }

        public function beforeSave(){
            $this->created_date = date('Y-m-d h:i:s');
        }
    }
    class PostulatedOffersChat extends Model {

        public $id;
        public $message;
        public $user;
        public $postulated_offer_id;
        public $created_date;

        public function initialize(){
            $this->setSource('postulated_offers_chat');
            $this->useDynamicUpdate(true);
            $this->keepSnapshots(true);
            $this->belongsTo(
                'postulated_offer_id',
                'PostulatedOffers',
                'id'
            );
            $this->belongsTo(
                'user',
                'Users',
                'id_users'
            );
            $this->status = 0;
            $this->created_date = date('Y-m-d');
        }

        public function beforeSave(){
            $this->created_date = date('Y-m-d');
        }
    }
?>