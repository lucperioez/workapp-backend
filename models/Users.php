<?php

    use Phalcon\Mvc\Model;
    use Phalcon\Mvc\Model\Message;
    use Phalcon\Validation;
    use Phalcon\Validation\Validator\Uniqueness;
    use Phalcon\Security;


    class UsersRoles extends Model{
        public $id_role;
        public $desciption;

        public function initialize(){
            $this->setSource('user_roles');
            $this->hasMany('id_role', 'Users', 'role_id');
        }
    }

    class Users extends Model {

        const ROLE_ADMIN = 1;
        const ROLE_CONTRATISTA = 2;
        const ROLE_EMPLEADO = 3;

        public $id_user;
        public $user;
        public $password;
        public $created_date;
        public $role_id;
        public $status;

        public function initialize(){
            $this->setSource('users');
            $this->useDynamicUpdate(true);
            $this->keepSnapshots(true);
            $this->created_date = date('Y-m-d h:i:s');
            $this->status = 1;
            $this->belongsTo(
                'role_id',
                'UsersRoles',
                'id_role'
            );
        }
        public function beforeSave(){
            $security = new Security();
            if(isset($this->id_user)){
                if($this->hasChanged('password')){
                    $this->password =  $security->hash($this->password);
                }
            }else{
                $this->password =  $security->hash($this->password);
            }
        }
        public function validation(){
            $validator = new Validation();
            $validator->add(
                'user',
                new Uniqueness([
                    'model' => $this,
                    'message' => 'Sorry, That username is already taken',
                ])
            );
    
            return $this->validate($validator);
        }
        public function comparePassword($password){
            $security = new Security();
            return $security->checkHash($password, $this->password);
        }

    }
    class UserDetail extends Model {

        public $id_detail;
        public $first_name;
        public $company_name;
        public $industry;
        public $last_name;
        public $phone;
        public $state;
        public $city;
        public $show_me;
        public $description;
        public $skill_rank;
        public $puntuality_rank;
        public $disciline_rank;
        public $bad_rank;
        public $experience;
        public $image;
        public $user_id;

        public function initialize(){
            $this->setSource('user_detail');
            $this->useDynamicUpdate(true);
            $this->keepSnapshots(true);
            $this->belongsTo('user_id','User','id_user');
        }
    }

    class EmployeeCities extends Model {

        public $id_employee_city;
        public $employee_id;
        public $city_id;

        public function initialize(){
            $this->setSource('employee_cities');
            $this->useDynamicUpdate(true);
            $this->keepSnapshots(true);
            $this->belongsTo('employee_id','UserDetail','id_detail');
        }
    }

    class References extends Model {

        public $id_reference;
        public $company;
        public $full_name;
        public $phone;
        public $description;
        public $created_date;
        public $employee_id;

        public function initialize(){
            $this->setSource('references');
            $this->useDynamicUpdate(true);
            $this->keepSnapshots(true);
            $this->belongsTo('employee_id','Users','id_user');
            $this->created_date = date('Y-m-d h:i:s');
        }
    }

    class UserWorkType extends Model {

        public $id;
        public $work_type;
        public $employee_id;

        public function initialize(){
            $this->setSource('user_worktype');
            $this->useDynamicUpdate(true);
            $this->keepSnapshots(true);
            $this->belongsTo('employee_id','Users','id_user');
        }
    }
?>