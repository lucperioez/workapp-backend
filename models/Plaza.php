<?php
    use Phalcon\Mvc\Model;
    use Phalcon\Mvc\Model\Message;
    use Phalcon\Validation;
    use Phalcon\Validation\Validator\Uniqueness;
    use Phalcon\Security;

    class Plaza extends Model {

        public $id_plaza;
        public $work_type;
        public $status;
        public $employe_id;
        public $skill_rank;
        public $puntuality_rank;
        public $dicipline_rank;
        public $bad_rank;
        public $end_hour;
        public $public_phone;
        public $offer_id;

        public function initialize(){
            $this->setSource('plazas');
            $this->useDynamicUpdate(true);
            $this->keepSnapshots(true);
            $this->belongsTo(
                'employe_id',
                'Users',
                'id_users'
            );
            $this->belongsTo(
                'offer_id',
                'Offers',
                'id_offer_offers'
            );
        } 
    }

?>