<?php

    use Phalcon\Mvc\Model;
    use Phalcon\Mvc\Model\Message;
    use Phalcon\Validation;
    use Phalcon\Validation\Validator\Uniqueness;
    use Phalcon\Security;

    class Adwords extends Model{
        public $id_adwords;
        public $url;
        public $action;

        public function initialize(){
            $this->setSource('adwords');
        }
    }

?>