<?php
    use Phalcon\Mvc\Model;
    use Phalcon\Mvc\Model\Message;
    use Phalcon\Validation;
    use Phalcon\Validation\Validator\Uniqueness;
    use Phalcon\Security;

    class Team extends Model {

        public $id_team;
        public $builder_id;
        public $employee_id;

        public function initialize(){
            $this->setSource('teams');
            $this->useDynamicUpdate(true);
            $this->keepSnapshots(true);
            $this->belongsTo(
                'builder_id',
                'Users',
                'id_users'
            );
        } 
        public function beforeDelete(){
            $ofers_teams = OfferTeam::find("team_id = $this->id_team");
            foreach ($ofers_teams as $value) {
                $value->delete();
            }
        }
    }



?>