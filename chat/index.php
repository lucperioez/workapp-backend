<?php

use Phalcon\Mvc\Micro;

include '../config.php';
include '../models/Offers.php';
include 'chat_postulated.php';

$app = new Micro();

$app->before(function() use ($app) {
	$app->response->setHeader("Access-Control-Allow-Origin", '*')
		->setHeader("Access-Control-Allow-Methods", 'GET,PUT,POST,DELETE,OPTIONS')
		->setHeader("Access-Control-Allow-Headers", 'Origin, X-Requested-With, Content-Range, Content-Disposition, Content-Type');	
	$app->response->sendHeaders();

});
$app->options('/{catch:(.*)}', function() use ($app) { 
	$app->response->setStatusCode(200, "OK")->send();
});

$app->get('/postulated/{id_postulated}','get_chat_postulated');
$app->post('/postulated','add_chat_postulated');


$app->notFound(function() use ($app){
	$app->response->setStatusCode(404, "Not Found");
	$app->response->sendHeaders();
	echo "That function is not here, call me, maybes!";

});

$app -> handle();