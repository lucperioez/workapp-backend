<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);


use Phalcon\Http\Request,
    Phalcon\Http\Response;

function get_chat_postulated($id_postulated){
    $status_code = array(200, 'Chat Data');
    $response = new Response(); 
    $chat = PostulatedOffersChat::find("postulated_offer_id='$id_postulated'");
        if($chat == false){
            $content = json_encode(array(
                'status'   => 'ERROR',
                'messages' => "Chat not found"
            ));
        }else{
            $content = json_encode(array(
                'status'   => 'OK',
                'messages' => "Chat found",
                'chat' =>   $chat
            ));
        }
    
    
    // Status Code
    $response->setStatusCode($status_code[0],$status_code[1]);
    // Mandamos el tipo de contenido al Header
    $response->setContentType('application/json');
    //Para acceder desde cualquier origen en el http request
    $response->setHeader('Access-Control-Allow-Origin', '*');
    // Asignamos los datos al contenido del header
    $response->setContent($content);
    // Regresamos la respuesta dada.
    return $response;
}


function add_chat_postulated(){
    
    $request = new Request(); // Get Data From Post
    $chat_data = (array) $request->getJsonRawBody(); // Convert Data To Object
    
    $nChat = new PostulatedOffersChat();
    $res = [];
    if($nChat->save($chat_data) === false) {
        $res =  [
            'status'   => 'ERROR',
        ];
        $messages = $nChat->getMessages();
        $res['message'] = $messages[0]->getMessage();
    }else{
        $res =  [
            'status'   => 'OK',
            'messages' => "New Message",
            'data' =>   $nChat
        ];
    }

    $status_code = array(200, 'New Message');
    $response = new Response(); 
    //Define the content 
    $content = json_encode($res);
    // Status Code
    $response->setStatusCode($status_code[0],$status_code[1]);
    // Mandamos el tipo de contenido al Header
    $response->setContentType('application/json');
    //Para acceder desde cualquier origen en el http request
    $response->setHeader('Access-Control-Allow-Origin', '*');
    // Asignamos los datos al contenido del header
    $response->setContent($content);
    // Regresamos la respuesta dada.
    return $response;

}
?>