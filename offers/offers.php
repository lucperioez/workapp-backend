<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

include '../models/Offers.php';

use Phalcon\Http\Request,
    Phalcon\Http\Response;

function get_offers($id_builder = -1){
    $status_code = array(200, 'Offer Data');
    $response = new Response(); 
    if($id_builder == null){
        $offers = Offers::find();
        $content = json_encode($offers);
    }else{
        $offers = Offers::find("builder_id = '$id_builder'");
       
        if($offers  == false){
            $content = json_encode(array(
                'status'   => 'ERROR',
                'messages' => "Offerts not found"
            ));
        }else{
            $content = json_encode(array(
                'status'   => 'OK',
                'messages' => "Offerts found",
                'offer' =>   $offers
            ));
        }
    }
    
    // Status Code
    $response->setStatusCode($status_code[0],$status_code[1]);
    // Mandamos el tipo de contenido al Header
    $response->setContentType('application/json');
    //Para acceder desde cualquier origen en el http request
    $response->setHeader('Access-Control-Allow-Origin', '*');
    // Asignamos los datos al contenido del header
    $response->setContent($content);
    // Regresamos la respuesta dada.
    return $response;
}

function get_offer($id_offer = -1){
    $status_code = array(200, 'Offer Data');
    $response = new Response(); 
    if($id_offer == null){
        $offers = Offers::find();
        $content = json_encode($offers);
    }else{
        $offer = Offers::findFirst($id_offer);
        if($offer == false){
            $content = json_encode(array(
                'status'   => 'ERROR',
                'messages' => "Offert not found"
            ));
        }else{
            $content = json_encode(array(
                'status'   => 'OK',
                'messages' => "Offert found",
                'offer' =>   $offer
            ));
        }
    }
    
    // Status Code
    $response->setStatusCode($status_code[0],$status_code[1]);
    // Mandamos el tipo de contenido al Header
    $response->setContentType('application/json');
    //Para acceder desde cualquier origen en el http request
    $response->setHeader('Access-Control-Allow-Origin', '*');
    // Asignamos los datos al contenido del header
    $response->setContent($content);
    // Regresamos la respuesta dada.
    return $response;
}


function add_offer(){
    
    $request = new Request(); // Get Data From Post
    $offer_data = (array) $request->getJsonRawBody(); // Convert Data To Object
    
    $nOffer = new Offers();
    $res = [];
    if($nOffer->save($offer_data) === false) {
        $res =  [
            'status'   => 'ERROR',
        ];
        $messages = $nOffer->getMessages();
        $res['message'] = $messages[0]->getMessage();
    }else{
        $res =  [
            'status'   => 'OK',
            'messages' => "Offer added",
            'offer' =>   $nOffer
        ];
    }

    $status_code = array(200, 'Offer Created');
    $response = new Response(); 
    //Define the content 
    $content = json_encode($res);
    // Status Code
    $response->setStatusCode($status_code[0],$status_code[1]);
    // Mandamos el tipo de contenido al Header
    $response->setContentType('application/json');
    //Para acceder desde cualquier origen en el http request
    $response->setHeader('Access-Control-Allow-Origin', '*');
    // Asignamos los datos al contenido del header
    $response->setContent($content);
    // Regresamos la respuesta dada.
    return $response;

}
function update_offer($id_offer){
    if($id_offer == null){
        exit(404);
    }
    $request = new Request(); // Get Data From Post
    $offer_data = (array) $request->getJsonRawBody(); // Convert Data To 
    $offer = Offers::findFirst($id_offer);
    $res = [];
    if($offer->update($offer_data) == false) {
        $res =  [
            'status'   => 'ERROR',
        ];
        $messages = $offer->getMessages();
        $res['message'] = $messages[0]->getMessage();
        
    }else{
        $res =  [
            'status'   => 'OK',
            'messages' => "Offer Updated",
            'offer' =>   $offer
        ];
    }
    $status_code = array(200, 'Offer Updated');
    $response = new Response(); 
    //Define the content 
    $content = json_encode($res);
    // Status Code
    $response->setStatusCode($status_code[0],$status_code[1]);
    // Mandamos el tipo de contenido al Header
    $response->setContentType('application/json');
    //Para acceder desde cualquier origen en el http request
    $response->setHeader('Access-Control-Allow-Origin', '*');
    // Asignamos los datos al contenido del header
    $response->setContent($content);
    // Regresamos la respuesta dada.
    return $response;

}

function delete_offer($id_offer){

    $status_code = array(200, 'Offer Deleted');
    $offer = Offers::findFirst($id_offer);
    $res = array();
    if ($offer !== false) {
        if ($offer->delete() === false) {
            
            $res['status']='ERROR';
            $messages = $offer->getMessages();
            $res['message'] = $messages[0]->getMessage();
            
        } else {

            $res['status'] = 'OK';
            $res['messages'] = "Offer Deleted";
        }
    }else{
            $res['status'] = 'ERROR';
            $res['messages'] = "Offer not found";
    
    }
    $response = new Response(); 
    //Define the content 
    $content = json_encode($res);
    // Status Code
    $response->setStatusCode($status_code[0],$status_code[1]);
    // Mandamos el tipo de contenido al Header
    $response->setContentType('application/json');
    //Para acceder desde cualquier origen en el http request
    $response->setHeader('Access-Control-Allow-Origin', '*');
    // Asignamos los datos al contenido del header
    $response->setContent($content);
    // Regresamos la respuesta dada.
    return $response;

}

function get_offers_status($status,$id_builder){
    $status_code = array(200, 'Offer Data');
    $response = new Response(); 
    if($status == null){
        $content = json_encode(array(
            'status'   => 'ERROR',
            'messages' => "Offerts not found"
        ));
    }else{
        $offers = Offers::find(["status = '$status'","builder_id = '$id_builder'"]);
       
        if($offers  == false){
            $content = json_encode(array(
                'status'   => 'ERROR',
                'messages' => "Offerts not found"
            ));
        }else{
            $content = json_encode(array(
                'status'   => 'OK',
                'messages' => "Offerts found",
                'offer' =>   $offers
            ));
        }
    }
    
    // Status Code
    $response->setStatusCode($status_code[0],$status_code[1]);
    // Mandamos el tipo de contenido al Header
    $response->setContentType('application/json');
    //Para acceder desde cualquier origen en el http request
    $response->setHeader('Access-Control-Allow-Origin', '*');
    // Asignamos los datos al contenido del header
    $response->setContent($content);
    // Regresamos la respuesta dada.
    return $response;
}
?>