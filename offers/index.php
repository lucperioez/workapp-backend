<?php

use Phalcon\Mvc\Micro;

include '../config.php';
include 'offers.php';
include 'postulated_offers.php';


$app = new Micro();

$app->before(function() use ($app) {
	$app->response->setHeader("Access-Control-Allow-Origin", '*')
		->setHeader("Access-Control-Allow-Methods", 'GET,PUT,POST,DELETE,OPTIONS')
		->setHeader("Access-Control-Allow-Headers", 'Origin, X-Requested-With, Content-Range, Content-Disposition, Content-Type');	
	$app->response->sendHeaders();

});
$app->options('/{catch:(.*)}', function() use ($app) { 
	$app->response->setStatusCode(200, "OK")->send();
});

$app->get('/{id_offer}','get_offer');
$app->get('/builder/{id_builder}','get_offers');
$app->post('/','add_offer');
$app->put('/{id_offer}','updaste_offer');
$app->delete('/{id_offer}','delete_offer');

$app->get('/status/{status}/{id_builder}','get_offers_status');

//Postulated offers
$app->get('/postulated/builder/{id_builder}','get_postulated_builder');
$app->get('/postulated/employe/{id_employe}','get_postulated_employe');
$app->get('/postulated/{id}','get_postulated');
$app->post('/postulated','add_postulated');
$app->put('/postulated/{id}','update_postulated');
$app->delete('/postulated/{id}','delete_postulated');
$app->post('/accepted/{id}','accepted_postulated');


$app->notFound(function() use ($app){
	$app->response->setStatusCode(404, "Not Found");
	$app->response->sendHeaders();
	echo "That function is not here, call me, maybes!";

});

$app -> handle();