<?php

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);


include '../models/Plaza.php';


use Phalcon\Http\Request,
    Phalcon\Http\Response;

function get_postulated_builder($id_builder){
    $status_code = array(200, 'Offer Data');
    $response = new Response(); 
    $offers_postulated = PostulatedOffers::find("builder_id = '$id_builder'");
    if($offers_postulated  == false){
            $content = json_encode(array(
                'status'   => 'ERROR',
                'messages' => "Offerts Postulated not found"
            ));
    }else{
        $content = json_encode(array(
            'status'   => 'OK',
            'messages' => "Offerts Postulated found",
            'data' =>   $offers_postulated
        ));
    }

    
    // Status Code
    $response->setStatusCode($status_code[0],$status_code[1]);
    // Mandamos el tipo de contenido al Header
    $response->setContentType('application/json');
    //Para acceder desde cualquier origen en el http request
    $response->setHeader('Access-Control-Allow-Origin', '*');
    // Asignamos los datos al contenido del header
    $response->setContent($content);
    // Regresamos la respuesta dada.
    return $response;
}

function get_postulated_employe($id_employe){
    $status_code = array(200, 'Offer Data');
    $response = new Response(); 
    $offers_postulated = PostulatedOffers::find("employe_id = '$id_employe'");
    if($offers_postulated  == false){
            $content = json_encode(array(
                'status'   => 'ERROR',
                'messages' => "Offerts Postulated not found"
            ));
    }else{
        $content = json_encode(array(
            'status'   => 'OK',
            'messages' => "Offerts Postulated found",
            'data' =>   $offers_postulated
        ));
    }

    
    // Status Code
    $response->setStatusCode($status_code[0],$status_code[1]);
    // Mandamos el tipo de contenido al Header
    $response->setContentType('application/json');
    //Para acceder desde cualquier origen en el http request
    $response->setHeader('Access-Control-Allow-Origin', '*');
    // Asignamos los datos al contenido del header
    $response->setContent($content);
    // Regresamos la respuesta dada.
    return $response;
}

function get_postulated($id){
    $status_code = array(200, 'Offer Data');
    $response = new Response(); 
    $offers_postulated = PostulatedOffers::findFirst("id = '$id'");
    if($offers_postulated  == false){
            $content = json_encode(array(
                'status'   => 'ERROR',
                'messages' => "Offerts Postulated not found"
            ));
    }else{
        $content = json_encode(array(
            'status'   => 'OK',
            'messages' => "Offerts Postulated found",
            'data' =>   $offers_postulated
        ));
    }

    
    // Status Code
    $response->setStatusCode($status_code[0],$status_code[1]);
    // Mandamos el tipo de contenido al Header
    $response->setContentType('application/json');
    //Para acceder desde cualquier origen en el http request
    $response->setHeader('Access-Control-Allow-Origin', '*');
    // Asignamos los datos al contenido del header
    $response->setContent($content);
    // Regresamos la respuesta dada.
    return $response;
}

function add_postulated(){
    
    $request = new Request(); // Get Data From Post
    $postulated_data = (array) $request->getJsonRawBody(); // Convert Data To Object
    
    $nPOffer = new PostulatedOffers();
    $res = [];
    if($nPOffer->save($postulated_data) === false) {
        $res =  [
            'status'   => 'ERROR',
        ];
        $messages = $nPOffer->getMessages();
        $res['message'] = $messages[0]->getMessage();
    }else{
        $res =  [
            'status'   => 'OK',
            'messages' => "Offer added",
            'data' =>   $nPOffer
        ];
    }

    $status_code = array(200, 'Offer Postulated Created');
    $response = new Response(); 
    //Define the content 
    $content = json_encode($res);
    // Status Code
    $response->setStatusCode($status_code[0],$status_code[1]);
    // Mandamos el tipo de contenido al Header
    $response->setContentType('application/json');
    //Para acceder desde cualquier origen en el http request
    $response->setHeader('Access-Control-Allow-Origin', '*');
    // Asignamos los datos al contenido del header
    $response->setContent($content);
    // Regresamos la respuesta dada.
    return $response;
}

function update_postulated($id){
    if($id == null){
        exit(404);
    }
    $request = new Request(); // Get Data From Post
    $postulated_data = (array) $request->getJsonRawBody(); // Convert Data To 
    $pOffer = PostulatedOffers::findFirst($id);
    $res = [];
    if($pOffer->update($postulated_data) == false) {
        $res =  [
            'status'   => 'ERROR',
        ];
        $messages = $pOffer->getMessages();
        $res['message'] = $messages[0]->getMessage();
        
    }else{
        $res =  [
            'status'   => 'OK',
            'messages' => "Offer Postulated Updated",
            'data' =>   $pOffer
        ];
    }
    $status_code = array(200, 'Offer Postulated Updated');
    $response = new Response(); 
    //Define the content 
    $content = json_encode($res);
    // Status Code
    $response->setStatusCode($status_code[0],$status_code[1]);
    // Mandamos el tipo de contenido al Header
    $response->setContentType('application/json');
    //Para acceder desde cualquier origen en el http request
    $response->setHeader('Access-Control-Allow-Origin', '*');
    // Asignamos los datos al contenido del header
    $response->setContent($content);
    // Regresamos la respuesta dada.
    return $response;

}

function delete_postulated($id){
    $status_code = array(200, 'Offer Deleted');
    $pOffer = PostulatedOffers::findFirst($id);
    $res = array();
    if ($pOffer !== false) {
        if ($pOffer->delete() === false) {
            
            $res['status']='ERROR';
            $messages = $pOffer->getMessages();
            $res['message'] = $messages[0]->getMessage();
            
        } else {

            $res['status'] = 'OK';
            $res['messages'] = "Offer Postulated Deleted";
        }
    }else{
            $res['status'] = 'ERROR';
            $res['messages'] = "Offer Postulated not found";
    
    }
    $response = new Response(); 
    //Define the content 
    $content = json_encode($res);
    // Status Code
    $response->setStatusCode($status_code[0],$status_code[1]);
    // Mandamos el tipo de contenido al Header
    $response->setContentType('application/json');
    //Para acceder desde cualquier origen en el http request
    $response->setHeader('Access-Control-Allow-Origin', '*');
    // Asignamos los datos al contenido del header
    $response->setContent($content);
    // Regresamos la respuesta dada.
    return $response;

}

function accepted_postulated($id){
    $status_code = array(200, 'Offer Accepted');
    //obtenemos la postulacion de la oferta
    $offers_postulated = PostulatedOffers::findFirst("id = '$id'");
    if($offers_postulated  == false){
        $content = json_encode(array(
            'status'   => 'ERROR',
            'messages' => "Offerts Postulated not found"
        ));
    }else{
        //actualizamos la postulacion
        $offers_postulated->status = 1;
        $offers_postulated->save();
        //buscamos si la oferta ya tiene cubierta
        $offer = Offers::findFirst("id_offer = '$offers_postulated->offer_id'");
        if($offer  == false){
            $content = json_encode(array(
                'status'   => 'ERROR',
                'messages' => "Offerts not found"
            ));
        }else{
            if($offer->completed == Offers::COMPLETED){
                $content = json_encode(array(
                    'status'   => 'ERROR',
                    'messages' => "Offerts completed"
                ));
            }else{
                //Creamos la plaza
                $nPlaza = new Plaza();
                $nPlaza->employee_id = $offers_postulated->employe_id;
                $nPlaza->offer_id = $offers_postulated->offer_id;
                if($nPlaza->save() != false) {
                    $content = json_encode(array(
                        'status'   => 'OK',
                        'messages' => "Plaza added",
                        'plaza' =>   $nPlaza
                    ));
                }
            }
        }
    }

    $response = new Response();
    // Status Code
    $response->setStatusCode($status_code[0],$status_code[1]);
    // Mandamos el tipo de contenido al Header
    $response->setContentType('application/json');
    //Para acceder desde cualquier origen en el http request
    $response->setHeader('Access-Control-Allow-Origin', '*');
    // Asignamos los datos al contenido del header
    $response->setContent($content);
    // Regresamos la respuesta dada.
    return $response;
}

?>