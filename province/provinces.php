<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);


use Phalcon\Http\Request,
    Phalcon\Http\Response;

function get_province($id_province = -1){
    $status_code = array(200, 'Province Data');
    $response = new Response();
    if($id_province == null){
        $provinces = Province::find();
        $content = json_encode($provinces);
    }else{
        $province = Province::findFirst($id_province);
        if($province == false){
            $content = json_encode(array(
                'status'   => 'ERROR',
                'messages' => "Province not found"
            ));
        }else{
            $content = json_encode(array(
                'status'   => 'OK',
                'messages' => "Province found",
                'province' =>   $province
            ));
        }
    }
    // Status Code
    $response->setStatusCode($status_code[0],$status_code[1]);
    // Mandamos el tipo de contenido al Header
    $response->setContentType('application/json');
    //Para acceder desde cualquier origen en el http request
    $response->setHeader('Access-Control-Allow-Origin', '*');
    // Asignamos los datos al contenido del header
    $response->setContent($content);
    // Regresamos la respuesta dada.
    return $response;
}

function get_province_by_status($status = 1){
    $status_code = array(200, 'Province Data');
    $response = new Response();
    $status = 1;
    $provinces = Province::find("status = $status");
    $content = json_encode($provinces);
    // Status Code
    $response->setStatusCode($status_code[0],$status_code[1]);
    // Mandamos el tipo de contenido al Header
    $response->setContentType('application/json');
    //Para acceder desde cualquier origen en el http request
    $response->setHeader('Access-Control-Allow-Origin', '*');
    // Asignamos los datos al contenido del header
    $response->setContent($content);
    // Regresamos la respuesta dada.
    return $response;
}

function add_province(){
    
    $request = new Request(); // Get Data From Post
    $province_data = (array) $request->getJsonRawBody(); // Convert Data To Object
    
    $nProvince = new Province();
    $res = [];
    if($nProvince->save($province_data) === false) {
        $res =  [
            'status'   => 'ERROR',
        ];
        $messages = $nProvince->getMessages();
        $res['message'] = $messages[0]->getMessage();
    }else{
        $res =  [
            'status'   => 'OK',
            'messages' => "Province added",
            'province' =>   $nProvince
        ];
    }

    $status_code = array(200, 'Province Created');
    $response = new Response(); 
    //Define the content 
    $content = json_encode($res);
    // Status Code
    $response->setStatusCode($status_code[0],$status_code[1]);
    // Mandamos el tipo de contenido al Header
    $response->setContentType('application/json');
    //Para acceder desde cualquier origen en el http request
    $response->setHeader('Access-Control-Allow-Origin', '*');
    // Asignamos los datos al contenido del header
    $response->setContent($content);
    // Regresamos la respuesta dada.
    return $response;

}
function update_province($id_province){
    if($id_province == null){
        exit(404);
    }
    $request = new Request(); // Get Data From Post
    $province_data = (array) $request->getJsonRawBody(); // Convert Data To 
    $province = Province::findFirst($id_province);
    $res = [];
    if($province->update($province_data) == false) {
        $res =  [
            'status'   => 'ERROR',
        ];
        $messages = $province->getMessages();
        $res['message'] = $messages[0]->getMessage();
        
    }else{
        $res =  [
            'status'   => 'OK',
            'messages' => "Province Updated",
            'province' =>   $province
        ];
    }
    $status_code = array(200, 'Province Updated');
    $response = new Response(); 
    //Define the content 
    $content = json_encode($res);
    // Status Code
    $response->setStatusCode($status_code[0],$status_code[1]);
    // Mandamos el tipo de contenido al Header
    $response->setContentType('application/json');
    //Para acceder desde cualquier origen en el http request
    $response->setHeader('Access-Control-Allow-Origin', '*');
    // Asignamos los datos al contenido del header
    $response->setContent($content);
    // Regresamos la respuesta dada.
    return $response;

}
function delete_province($id_province){

    $status_code = array(200, 'Province Disabled');
    $province = Province::findFirst($id_province);
    $res = array();
    if ($province !== false) {
        $province->status = 0;
        if ($province->save() === false) {
            
            $res['status']='ERROR';
            $messages = $province->getMessages();
            $res['message'] = $messages[0]->getMessage();
            
        } else {
            $cities = City::find("province_id = '$id_province'");
            foreach ($cities as $city) {
                $city->status = 0;
                $city->save();
            }
            $res['status'] = 'OK';
            $res['messages'] = "Province and Cities Disabled";
        }
    }else{
            $res['status'] = 'ERROR';
            $res['messages'] = "Province not found";
    
    }
    $response = new Response(); 
    //Define the content 
    $content = json_encode($res);
    // Status Code
    $response->setStatusCode($status_code[0],$status_code[1]);
    // Mandamos el tipo de contenido al Header
    $response->setContentType('application/json');
    //Para acceder desde cualquier origen en el http request
    $response->setHeader('Access-Control-Allow-Origin', '*');
    // Asignamos los datos al contenido del header
    $response->setContent($content);
    // Regresamos la respuesta dada.
    return $response;

}

/*function delete_province($id_province){

    $status_code = array(200, 'Province Deleted');
    $province = Province::findFirst($id_province);
    $res = array();
    if ($province !== false) {
        if ($province->delete() === false) {
            
            $res['status']='ERROR';
            $messages = $province->getMessages();
            $res['message'] = $messages[0]->getMessage();
            
        } else {

            $res['status'] = 'OK';
            $res['messages'] = "Province Deleted";
        }
    }else{
            $res['status'] = 'ERROR';
            $res['messages'] = "Province not found";
    
    }
    $response = new Response(); 
    //Define the content 
    $content = json_encode($res);
    // Status Code
    $response->setStatusCode($status_code[0],$status_code[1]);
    // Mandamos el tipo de contenido al Header
    $response->setContentType('application/json');
    //Para acceder desde cualquier origen en el http request
    $response->setHeader('Access-Control-Allow-Origin', '*');
    // Asignamos los datos al contenido del header
    $response->setContent($content);
    // Regresamos la respuesta dada.
    return $response;

}*/

?>