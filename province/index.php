<?php

use Phalcon\Mvc\Micro;

include '../config.php';
include '../models/Cities.php';
include 'cities.php';
include 'provinces.php';


$app = new Micro();

$app->before(function() use ($app) {
	$app->response->setHeader("Access-Control-Allow-Origin", '*')
		->setHeader("Access-Control-Allow-Methods", 'GET,PUT,POST,DELETE,OPTIONS')
		->setHeader("Access-Control-Allow-Headers", 'Origin, X-Requested-With, Content-Range, Content-Disposition, Content-Type');	
	$app->response->sendHeaders();

});
$app->options('/{catch:(.*)}', function() use ($app) { 
	$app->response->setStatusCode(200, "OK")->send();
});

$app->get('/{id_province}','get_province');
$app->get('/status/{status}','get_province_by_status');
$app->post('/','add_province');
$app->put('/{id_province}','update_province');
$app->delete('/{id_province}','delete_province');

$app->get('/city/{id_province}','get_cities');
$app->get('/city/status/{status}','get_cities_by_status');
$app->post('/city','add_city');
$app->put('/city/{id_city}','update_city');
$app->delete('/city/{id_city}','delete_city');

$app->notFound(function() use ($app){
	$app->response->setStatusCode(404, "Not Found");
	$app->response->sendHeaders();
	echo "That function is not here, call me, maybes!";

});

$app -> handle();