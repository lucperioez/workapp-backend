<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);



use Phalcon\Http\Request,
    Phalcon\Http\Response;

function get_cities($id_province = -1){
    $status_code = array(200, 'City Data');
    $response = new Response(); 
    $cities = City::find("province_id = '$id_province'");
    $content = json_encode($cities);
    // Status Code
    $response->setStatusCode($status_code[0],$status_code[1]);
    // Mandamos el tipo de contenido al Header
    $response->setContentType('application/json');
    //Para acceder desde cualquier origen en el http request
    $response->setHeader('Access-Control-Allow-Origin', '*');
    // Asignamos los datos al contenido del header
    $response->setContent($content);
    // Regresamos la respuesta dada.
    return $response;
}

function get_cities_by_status($status = -1){
    $status_code = array(200, 'City Data');
    $response = new Response(); 
    $cities = City::find("status = '$status'");
    $content = json_encode($cities);
    // Status Code
    $response->setStatusCode($status_code[0],$status_code[1]);
    // Mandamos el tipo de contenido al Header
    $response->setContentType('application/json');
    //Para acceder desde cualquier origen en el http request
    $response->setHeader('Access-Control-Allow-Origin', '*');
    // Asignamos los datos al contenido del header
    $response->setContent($content);
    // Regresamos la respuesta dada.
    return $response;
}

function add_city(){
    
    $request = new Request(); // Get Data From Post
    $city_data = (array) $request->getJsonRawBody(); // Convert Data To Object
    
    $nCity = new City();
    $res = [];
    if($nCity->save($city_data) === false) {
        $res =  [
            'status'   => 'ERROR',
        ];
        $messages = $nCity->getMessages();
        $res['message'] = $messages[0]->getMessage();
    }else{
        $res =  [
            'status'   => 'OK',
            'messages' => "City added",
            'city' =>   $nCity
        ];
    }

    $status_code = array(200, 'City Created');
    $response = new Response(); 
    //Define the content 
    $content = json_encode($res);
    // Status Code
    $response->setStatusCode($status_code[0],$status_code[1]);
    // Mandamos el tipo de contenido al Header
    $response->setContentType('application/json');
    //Para acceder desde cualquier origen en el http request
    $response->setHeader('Access-Control-Allow-Origin', '*');
    // Asignamos los datos al contenido del header
    $response->setContent($content);
    // Regresamos la respuesta dada.
    return $response;

}
function update_city($id_city){
    if($id_city == null){
        exit(404);
    }
    $request = new Request(); // Get Data From Post
    $city_data = (array) $request->getJsonRawBody(); // Convert Data To 
    $city = City::findFirst($id_city);
    $res = [];
    if($city->update($city_data) == false) {
        $res =  [
            'status'   => 'ERROR',
        ];
        $messages = $city->getMessages();
        $res['message'] = $messages[0]->getMessage();
        
    }else{
        $res =  [
            'status'   => 'OK',
            'messages' => "Offer Updated",
            'city' =>   $city
        ];
    }
    $status_code = array(200, 'City Updated');
    $response = new Response(); 
    //Define the content 
    $content = json_encode($res);
    // Status Code
    $response->setStatusCode($status_code[0],$status_code[1]);
    // Mandamos el tipo de contenido al Header
    $response->setContentType('application/json');
    //Para acceder desde cualquier origen en el http request
    $response->setHeader('Access-Control-Allow-Origin', '*');
    // Asignamos los datos al contenido del header
    $response->setContent($content);
    // Regresamos la respuesta dada.
    return $response;

}

function delete_city($id_city){

    $status_code = array(200, 'City Disabled');
    $city = City::findFirst($id_city);
    $res = array();
    if ($city !== false) {
        $city->status = 0;
        if ($city->save() === false) {
            
            $res['status']='ERROR';
            $messages = $city->getMessages();
            $res['message'] = $messages[0]->getMessage();
            
        } else {

            $res['status'] = 'OK';
            $res['messages'] = "City Disabled";
        }
    }else{
            $res['status'] = 'ERROR';
            $res['messages'] = "City not found";
    
    }
    $response = new Response(); 
    //Define the content 
    $content = json_encode($res);
    // Status Code
    $response->setStatusCode($status_code[0],$status_code[1]);
    // Mandamos el tipo de contenido al Header
    $response->setContentType('application/json');
    //Para acceder desde cualquier origen en el http request
    $response->setHeader('Access-Control-Allow-Origin', '*');
    // Asignamos los datos al contenido del header
    $response->setContent($content);
    // Regresamos la respuesta dada.
    return $response;

}

/*function delete_city($id_city){

    $status_code = array(200, 'City Deleted');
    $city = City::findFirst($id_city);
    $res = array();
    if ($city !== false) {
        if ($city->delete() === false) {
            
            $res['status']='ERROR';
            $messages = $city->getMessages();
            $res['message'] = $messages[0]->getMessage();
            
        } else {

            $res['status'] = 'OK';
            $res['messages'] = "City Deleted";
        }
    }else{
            $res['status'] = 'ERROR';
            $res['messages'] = "City not found";
    
    }
    $response = new Response(); 
    //Define the content 
    $content = json_encode($res);
    // Status Code
    $response->setStatusCode($status_code[0],$status_code[1]);
    // Mandamos el tipo de contenido al Header
    $response->setContentType('application/json');
    //Para acceder desde cualquier origen en el http request
    $response->setHeader('Access-Control-Allow-Origin', '*');
    // Asignamos los datos al contenido del header
    $response->setContent($content);
    // Regresamos la respuesta dada.
    return $response;

}*/

?>